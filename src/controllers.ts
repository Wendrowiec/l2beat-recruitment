import { Context, Next } from 'koa';
import CoefficientsSchema from './schema';
import { solveLinearEquation, solveQuadraticEquation } from './utils';

const quadraticController = async (ctx: Context, next: Next) => {
    try {
        const {a, b, c} = CoefficientsSchema.parse(ctx.query);
        if(a === 0) {
            ctx.body = {
                solution: solveLinearEquation(b, c)
            }
        }
        else {
            ctx.body = {
                solution: solveQuadraticEquation(a,b, c)
            }
        }
        ctx.status = 200;
    } 
    catch(error) {
        ctx.status = 400;
        ctx.message = "Invalid query parameters";
    }
    await next();
};

export default quadraticController;
