import * as Router from '@koa/router';
import quadraticController from './controllers';

const router = new Router();

router.get('/quadratic', quadraticController);

export default router;
