import {z} from 'zod';

const numericString = z.preprocess(
    (a) => parseInt(a as string, 10),
    z.number()
);

const CoefficientsSchema = z.object({
    a: numericString,
    b: numericString,
    c: numericString
});

export default CoefficientsSchema;
