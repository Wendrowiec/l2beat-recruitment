import * as Koa from 'koa';
import router from './router';

const server = new Koa();

server.use(router.routes()).use(router.allowedMethods());

export default server.listen(3000);
