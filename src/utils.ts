export const solveLinearEquation = (linearCoefficient: number, coefficient: number): string | number[] => {
    if(linearCoefficient === 0) {
        if(coefficient === 0) {
            return "Every real number";
        }
        else {
            return "The equation is contradictory";
        }
    }
    else {
        return [- coefficient / linearCoefficient];
    }
};

export const solveQuadraticEquation = (quadraticCoefficient: number, linearCoefficient: number, coefficient: number): string | number[] => {
    const delta = linearCoefficient * linearCoefficient - 4 * quadraticCoefficient * coefficient;

    if (delta < 0) {
        return "There are no real solutions";
    } 
    else {
        const solutions = new Set<number>();

        solutions.add((-linearCoefficient + Math.sqrt(delta)) / (2 * quadraticCoefficient));
        solutions.add((-linearCoefficient - Math.sqrt(delta)) / (2 * quadraticCoefficient));

        return Array.from(solutions);
    }
};
