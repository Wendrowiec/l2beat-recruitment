import * as chai from 'chai';
import chaiHttp = require('chai-http');
import server from '../src';

chai.use(chaiHttp);
const expect = chai.expect;

describe('Qudratic', () => {
    const endpointRoute = "/quadratic";

    const constructUrl = (a: number, b: number, c: number) => {
        return `${endpointRoute}?a=${a}&b=${b}&c=${c}`;
    };

    const badRequestErrorMessage = "Invalid query parameters";

    it('should return 400 when no query parameters are provided', (done) => {
        chai.request(server).get(endpointRoute).end((_, response) => {
            expect(response).to.have.status(400);
            expect(response.error.text).to.equal(badRequestErrorMessage);
            done();
        });
    });

    it('should return 400 when query parameters are invalid', (done) => {
        chai.request(server).get(`${endpointRoute}?a=10&b=siema&c=eniu`).end((_, response) => {
            expect(response).to.have.status(400);
            expect(response.error.text).to.equal(badRequestErrorMessage);
            done();
        });
    });

    it('should return no real solutions for a = 1 b = 2 c = 3', (done) => {
        chai.request(server).get(constructUrl(1, 2, 3)).end((_, response) => {
            expect(response).to.have.status(200);
            expect(response.body).to.deep.equal({solution: "There are no real solutions"});
            done();
        });
    });

    it('should return contradictory equation for a = 0 b = 0 c = 5', (done) => {
        chai.request(server).get(constructUrl(0, 0, 5)).end((_, response) => {
            expect(response).to.have.status(200);
            expect(response.body).to.deep.equal({solution: "The equation is contradictory"});
            done();
        });
    });

    it('should return every real number for a = 0 b = 0 c = 0', (done) => {
        chai.request(server).get(constructUrl(0, 0, 0)).end((_, response) => {
            expect(response).to.have.status(200);
            expect(response.body).to.deep.equal({solution: "Every real number"});
            done();
        });
    });

    it('should return -0.5 for a = 0 b = 2 c = 1', (done) => {
        chai.request(server).get(constructUrl(0, 2, 1)).end((_, response) => {
            expect(response).to.have.status(200);
            expect(response.body).to.deep.equal({solution: [-0.5]});
            done();
        });
    });

    it('should return -2 and -3 for a = 1 b = 5 c = 6', (done) => {
        chai.request(server).get(constructUrl(1, 5, 6)).end((_, response) => {
            expect(response).to.have.status(200);
            expect(response.body).to.deep.equal({solution: [-2, -3]});
            done();
        });
    });

    it('should return 2 for a = 1 b = -4 c = 4', (done) => {
        chai.request(server).get(constructUrl(1, -4, 4)).end((_, response) => {
            expect(response).to.have.status(200);
            expect(response.body).to.deep.equal({solution: [2]});
            done();
        });
    });
})
